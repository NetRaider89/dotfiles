.PHONY: vim

vim: 
	ln -s $(shell pwd)/vim/.vimrc ~/.vimrc
	ln -s $(shell pwd)/vim/.vim ~/.vim
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	vim +PluginInstall +qall
	$(shell pwd)/vim/.vim/bundle/YouCompleteMe/install.py
