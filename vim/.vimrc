set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'klen/python-mode'
Plugin 'jpalardy/vim-slime'
Plugin 'tomasr/molokai'
call vundle#end()

filetype plugin indent on
set t_Co=256        " enable 256-colors mode
set number          " show line numbers
syntax enable       " enable syntax highlighting
set background=dark " dark background colorscheme
colorscheme molokai " set colorscheme
set colorcolumn=79  " lines longer than 79 columns will be broken
set shiftwidth=4    " operation >> indents 4 columns; << unindents 4 columns
set tabstop=4       " a hard TAB displays as 4 columns
set expandtab       " insert spaces when hitting TABs
set softtabstop=4   " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set shiftround      " round indent to multiple of 'shiftwidth'
set autoindent      " align the new line indent with the previous line
set splitright      " vsplit creates a new vertical split to the right
set splitbelow      " split creates a new horizontal split below

inoremap jk <esc>

" Python-mode configuration, for now I want only text objects
let g:pymode = 1                    " enable python-mode
let g:pymode_options = 0            " vim options are handled separately
let g:pymode_indent = 0             " disable pymode indent
let g:pymode_folding = 1
let g:pymode_motion = 1             " python-mode text objects
let g:pymode_rope = 0               " may conflict woth YCM
let g:pymode_rope_completion = 0    " WILL conflict with YCM
let g:pymode_syntax = 0             " it's slow

" vim-slime configuration
let g:slime_target = "tmux"         " set tmux mode 
let g:slime_python_ipython = 1      " use %cpaste when sending data to ipython
